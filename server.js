const Web3 = require("web3");
const rp = require("request-promise")
const dotenv = require('dotenv')
var Tx = require("ethereumjs-tx");
const myContract = require("./build/contracts/Erc20Token.json")
// const cors = require("cors");
const express = require("express");

const contractAddress = myContract.networks["3"].address;
const app = express();

dotenv.config({path:"./config.env"})

// app.options("*", cors());
// app.use(cors());

app.use(express.json());
app.use(
  express.urlencoded({
    extended: false,
  })
);

const infuraUrl = process.env.INFURA_URL;
const web3 = new Web3(infuraUrl);
const contract = new web3.eth.Contract(myContract.abi, contractAddress);

async function loadBlockchainData(account) {
  try {
    const transferTx = await contract.getPastEvents("Transfer", {
      fromBlock: 0,
      toBlock: "latest",
      filter: { from: account },
    });
    const buyTx = await contract.getPastEvents("Buy", {
      fromBlock: 0,
      toBlock: "latest",
      filter: { from: account },
    });
    const withdrawTx = await contract.getPastEvents("Withdraw", {
      fromBlock: 0,
      toBlock: "latest",
      filter: { from: account },
    });
    const _balance = await contract.methods.balanceOf(account).call();
    const balance = web3.utils.fromWei(_balance.toString(), "Ether");
    const obj = {
      account,
      events:{transferTx,buyTx,withdrawTx},
      balance,
    };
    return obj;
  } catch (error) {
    console.log(error);
    return error;
  }
}

async function transfer(privateKey, account, recipient, amount) {
  const privatekeyBuffer = Buffer.from(privateKey, "hex");
  const networkId = await web3.eth.net.getId();
  const txCount = await web3.eth.getTransactionCount(account);
  const transaction = contract.methods.transfer(account,recipient, amount)
  const txObject = {
    to: contractAddress,
    from: account,
    gasLimit: await transaction.estimateGas({from:account}),
    gasPrice: web3.utils.toHex(await web3.eth.getGasPrice()),
    data: transaction.encodeABI(),
    nonce: web3.utils.toHex(txCount),
    chainId: networkId,
  };

  const tx = new Tx.Transaction(txObject, { chain: "ropsten" });

  tx.sign(privatekeyBuffer);

  const serializeTx = tx.serialize();
  const raw = "0x" + serializeTx.toString("hex");
  return raw;
}


async function withdraw(privateKey, account) {
  
  const privatekeyBuffer = Buffer.from(privateKey, "hex");
  const networkId = await web3.eth.net.getId();
  const txCount = await web3.eth.getTransactionCount(account);
  const txObject = {
    to: contractAddress,
    from: account,
    gasLimit: web3.utils.toHex(2100000),
    gasPrice: web3.utils.toHex(web3.utils.toWei("6", "gwei")),
    data: contract.methods.withdraw().encodeABI(),
    nonce: web3.utils.toHex(txCount),
    chainId: networkId,
  };

  const tx = new Tx.Transaction(txObject, { chain: "ropsten" });

  tx.sign(privatekeyBuffer);

  const serializeTx = tx.serialize();
  const raw = "0x" + serializeTx.toString("hex");
  return raw;
}


async function buyToken(privateKey, account, eth) {
 
  const privatekeyBuffer = Buffer.from(privateKey, "hex");
  const networkId = await web3.eth.net.getId();
  const txCount = await web3.eth.getTransactionCount(account);
  const _eth = eth.toString()
  const txObject = {
    to: contractAddress,
    from: account,
    value:  web3.utils.toHex(web3.utils.toWei(_eth, 'ether')),
    gasLimit: web3.utils.toHex(2100000),
    gasPrice: web3.utils.toHex(web3.utils.toWei("6", "gwei")),
    data: contract.methods.buyToken().encodeABI(),
    nonce: web3.utils.toHex(txCount),
    chainId: networkId,
  };

  const tx = new Tx.Transaction(txObject, { chain: "ropsten" });

  tx.sign(privatekeyBuffer);

  const serializeTx = tx.serialize();
  const raw = "0x" + serializeTx.toString("hex");
  return raw;
}


async function createAccount(password){
  try{
  const account = await web3.eth.accounts.create()
  const encrypted_pk =  await web3.eth.accounts.encrypt(account.privateKey,password)
  // const decrypt_pk = await web3.eth.accounts.decrypt(encrypted_pk,password)
  const obj = {address:account.address,encrypted_pk}
  return obj
  }
  catch(err){

  }
  // console.log("private key ===>",decrypt_pk.privateKey)
}

app.post("/createAccount", async (req, res) => {
  try{
  const password = req.body.password;

  let result = await createAccount(password)
  
  res.send(result);
  } catch(err){
console.log("===>",err)
return err
  }
});
app.get("/:account", async (req, res) => {
  try {
    const account = req.params.account;
    let response = await loadBlockchainData(account);

    res.send(response);
  } catch (err) {
    console.log(err);
    return err;
  }
});

app.post("/transfer", async (req, res) => {
  var recipient = req.body.recipient;
  var amount = req.body.amount;
  var privateKey = req.body.privateKey;
  var account = req.body.account;
  const result = await transfer(privateKey, account, recipient, amount);
  web3.eth
    .sendSignedTransaction(result)
    .on("transactionHash", (txHash) => res.json({ txHash }))
    .on("error", console.log);
});

app.post("/buyToken", async (req, res) => {
  var eth = req.body.eth;
  var privateKey = req.body.privateKey;
  var account = req.body.account;
  const result = await buyToken(privateKey, account, eth);
  web3.eth
    .sendSignedTransaction(result)
    .on("transactionHash", (txHash) => res.json({ txHash }))
    .on("error", console.log);
});

app.get("/api/getUSDRate", async (req, res) => {
  var options = {
    uri: "https://min-api.cryptocompare.com/data/price",
    qs: {
      fsym:"ETH",
      tsyms:"USD"
    },
    headers: {
        'User-Agent': 'Request-Promise'
    },
    json: true // Automatically parses the JSON string in the response
  };
    const response = await rp(options)
     res.json({usd:response.USD})
})

app.post("/withdraw", async (req, res) => {
  var privateKey = req.body.privateKey;
  var account = req.body.account;
  const result = await withdraw(privateKey, account);
  web3.eth
    .sendSignedTransaction(result)
    .on("transactionHash", (txHash) => res.json({ txHash }))
    .on("error", console.log);
});



const PORT = process.env.PORT || 4000; 
app.listen(PORT, () => console.log(`Server started on port ${PORT}`));


