//SPDX-License-Identifier: UNLICENSED   
pragma solidity ^0.6.3;


contract Erc20Token{
    
    string public tokenName;
    string public tokenSymbol;
    uint256 public tokenDecimals;
    uint256 private initialSupply;
    address private owner;
    uint256 public basePrice;

    mapping (address => uint) pendingWithdrawals;
    mapping (address => uint256) private balances;
    mapping(address => mapping(address => uint256)) public allowance;
    
    constructor(string memory _name,string memory _symbol,uint256 _basePrice,uint256 _initialSupply) 
    public
    {
        tokenName = _name;
        tokenSymbol = _symbol;
        tokenDecimals = 18;
        basePrice = _basePrice*10**tokenDecimals;
        initialSupply = _initialSupply*10**tokenDecimals;
        owner = msg.sender;
        balances[owner] = initialSupply;
    }
    
    event Approval(address _owner, address _to, uint256 _value);
    event Transfer(address indexed from, address indexed to, uint256 tokens);
    event Buy(address indexed from, address indexed to, uint256 tokens,uint256 eth);
    event Withdraw(address indexed from, address indexed to, uint256 tokens,uint256 eth);
    
    modifier onlyOwner() {
        require(msg.sender == owner,"only owner can run this function");
            _;
    }
    
    function transfer(address _from,address to, uint256 tokens) public returns (bool) {
        require(_from != address(0), "ERC20: transfer from the zero address");
        require(to != address(0), "ERC20: transfer to the zero address");
        require(tokens <= balances[_from], "you dont have enough tokens");
        require(_from != to,"sender and recipient address can't be same");
        balances[_from] -= tokens;
        balances[to] += tokens;
        emit Transfer(_from, to, tokens);
        return true;
    }
    
    function increaseInitialSupply(uint256 _initialSupply) public{
        initialSupply += _initialSupply; 
        balances[owner] += _initialSupply;
    }
    
    function approved(address _spender, uint256 _value)
        public
        returns (bool success)
    {
        allowance[msg.sender][_spender] = _value;
        emit Approval(msg.sender, _spender, _value);
        return true;
    }
    
    function transferFrom(address _from, address _to, uint256 _value)public returns(bool success){
       require(_value <= balances[_from], 'not sufficient blance');
       require(_value <= allowance[_from][msg.sender],'allowance should be maintained');
        balances[_from] -= _value;
        balances[_to] += _value;

        allowance[_from][msg.sender] -= _value;

        emit Transfer(_from, _to, _value);

        return(true); 
    }
    
    function balanceOf(address tokenOwner) public view returns (uint256) {
        return balances[tokenOwner];
    }
    
    function adjustPrice(uint256 _newPrice) view public onlyOwner() {
        basePrice == _newPrice;
        
    }

    function buyToken() public payable {
        require(msg.value <= basePrice,"you dont have enough ether to buy Token. you should have atleast 0.01 eth to buy 1 token");
        require(msg.value > 0 ether, "invailed amount");
        require(tx.origin == msg.sender,"should be external owned account");
        require(msg.sender != address(0), "buyer should have EOA");
        
        uint wei_unit = (1*10**tokenDecimals)/basePrice;
        uint final_price = msg.value * wei_unit;
        pendingWithdrawals[msg.sender] += msg.value;
        emit Buy(msg.sender,owner,final_price,msg.value);
        transfer(owner,msg.sender, final_price);
       
    }
    
    function withdraw() public returns(bool success){
        uint amount = pendingWithdrawals[msg.sender];
        pendingWithdrawals[msg.sender] = 0;
        msg.sender.transfer(amount);
        uint256 tokenBalance = balanceOf(msg.sender); 
        transfer(msg.sender,owner,tokenBalance);
        emit Withdraw(msg.sender, owner, tokenBalance,amount);
        return true;
    }

    receive() external payable {
        buyToken();
    }

    fallback() external payable {
        buyToken();
    }
}
